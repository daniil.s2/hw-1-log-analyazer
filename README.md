Log Analyzer
===============

**Overview**
***
Home work for course Otus “Python Developer”<br>
Lesson: 01_advanced_basics<br>
Task: Log Analyzer

This script analyze nginx log file which has format:
```nginx
$remote_addr $remote_user $http_x_real_ip [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" $request_time
```

**Requirements:**
***
Linux Mint 19<br>
Python 3.6.7<br>

**Usage:**
***
1. Command line mode<br>
**python log_analyzer.py**

2. Config file mode<br>
**python log_analyzer.py --config path_config**
