#!/usr/bin/env python

import logging
import argparse
import gzip
import json
import re
import os
from collections import namedtuple
from datetime import datetime
from json import JSONDecodeError
from statistics import median
from string import Template
from tempfile import NamedTemporaryFile


# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';


CONFIG_PATH = './config.json'
REPORT_DUMMY_PATH = './data/report_dummy.html'
LOGGER = logging.getLogger(__file__)

config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log"
}

LogInfo = namedtuple('LogInfo', ['path', 'date'])


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", action="store", help="set config path")
    return parser.parse_args()


def load_config(path, default_conf):
    
    with open(path, 'r', encoding='UTF-8') as f:
        data = json.load(f)
    config = dict(default_conf)
    config.update(data)

    return config


def get_last_log(log_dir):
    LOGGER.debug('Getting last log...')
    LOGGER.debug('Log directory: {}'.format(log_dir))

    if not os.path.isdir(log_dir):
        return None

    last_date = None
    last_path = None

    re_date = '(?P<date>\d{8})'
    re_ext = '(?:|\.[Gg][Zz])'
    re_path = r'^nginx-access-ui\.log-{}{}$'.format(re_date, re_ext)

    for f_name in os.listdir(log_dir):

        f_path = os.path.join(log_dir, f_name)

        if not os.path.isfile(f_path):
            continue

        f_date = re.search(re_path, f_name)
        f_date = f_date.group('date') if f_date else None
        if not f_date:
            continue
        else:
            try:
                f_date = datetime.strptime(f_date, '%Y%m%d').date()
            except ValueError:
                continue

        if f_date and (not last_date or last_date < f_date):
            last_date = f_date
            last_path = f_path

    if last_path and last_date:
        return LogInfo(last_path, last_date)


def is_exist_report(report_dir, date):
    LOGGER.debug('Checking report for existence...')

    report_path = 'report-{}.html'.format(date.strftime('%d.%m.%Y'))
    report_path = os.path.join(report_dir, report_path)

    LOGGER.debug('Report path: {}'.format(report_path))

    return os.path.isfile(report_path)


def parse_log(raw_str):
    re_url_path = r'(?P<path>(?:\/[^\/\?#\[\]\s]+)+\/?(?=\?|\#|$))'
    re_url_query = r'(?P<query>(?:\?[^$#\[\]\s]*?(?=$|\#))?)'
    re_url_fragment = r'(?P<fragment>\#[^\s]*)?'
    re_url = r'^{url_path}{url_query}{url_fragment}$'.format(
        url_path=re_url_path,
        url_query=re_url_query,
        url_fragment=re_url_fragment
    )

    request = raw_str.split('"')
    request = request[1] if len(request) >= 3 else None
    url = request.split() if request else None
    url = url[1] if url and len(url) >= 2 else None
    if not url or not re.match(re_url, url):
        return

    request_time = raw_str.split()
    request_time = request_time[-1] if len(request_time) else None

    if request_time is None:
        return

    try:
        request_time = float(request_time)
    except ValueError:
        return

    return url, request_time


def f_reader(f_path):
    if f_path.endswith('.gz'):
        return gzip.open(f_path, 'rt', encoding='UTF-8')
    else:
        return open(f_path, 'r', encoding='UTF-8')


def extract_log_data(log_path, parser, error_limit=65):
    LOGGER.debug('Start extracting data from log...')
    LOGGER.debug('Log path: {}'.format(log_path))

    error_count = 0
    count = 0

    with f_reader(log_path) as log_f:
        for row in log_f:
            count += 1
            result = parser(row)

            if not result:
                error_count += 1
                continue

            yield result

        error_percent = int((error_count * 100) / count)
        if error_percent > error_limit:
            LOGGER.error('Parsing error limit exceeded!')
            raise Exception


def prepare_log_statistic(log_data, report_size):
    LOGGER.debug('Start calculating statistic...')

    urls_data = dict()
    results = list()

    for url, request_time in log_data:

        data = urls_data.get(url, dict())
        data.setdefault('url', url)
        data.setdefault('time_values', list()).append(request_time)
        urls_data[url] = data

    for data in urls_data.values():
        time_values = data['time_values']

        data['count'] = len(time_values)
        data['time_sum'] = round(sum(time_values), 3)

        time_avg = data['time_sum'] / data['count']
        data['time_avg'] = round(time_avg, 3)

        data['time_max'] = round(max(time_values), 3)
        data['time_med'] = round(median(time_values), 3)

    total_time_sum = sum(i['time_sum'] for i in urls_data.values())

    total_count = len(urls_data)
    for data in urls_data.values():
        data['time_sum'] = round(data['time_sum'], 3)

        count_perc = (data['count'] * 100) / total_count
        data['count_perc'] = round(count_perc, 3)

        time_perc = (data['time_sum'] * 100) / total_time_sum
        data['time_perc'] = round(time_perc, 3)

    for data in urls_data.values():
        results.append(
            {
                'url': data['url'],
                'count': data['count'],
                'time_sum': data['time_sum'],
                'time_avg': data['time_avg'],
                'time_max': data['time_max'],
                'time_med': data['time_med'],
                'count_perc': data['count_perc'],
                'time_perc': data['time_perc']
            }
        )

    results = sorted(
        results, key=lambda v: v['time_sum'], reverse=True
    )

    return results[:report_size]


def create_report(data, report_dir, report_date):
    LOGGER.debug('Start creating report...')

    dummy = None
    report = None

    if not os.path.isdir(report_dir):
        try:
            os.mkdir(report_dir)
            LOGGER.debug('Created report directory.')
            LOGGER.debug('Report directory path: {}'.format(report_dir))
        except FileExistsError:
            pass

    report_path = 'report-{}.html'.format(report_date.strftime('%d.%m.%Y'))
    report_path = os.path.join(report_dir, report_path)
    LOGGER.debug('Report path: {}'.format(report_path))

    dummy = open(REPORT_DUMMY_PATH, 'r', encoding='UTF-8')

    # create tmp file
    report = NamedTemporaryFile(mode='w',
                                encoding='UTF-8',
                                dir=report_dir,
                                delete=False)

    try:
        report_data = Template(dummy.read())
        report_data = report_data.safe_substitute(table_json=data)
        report.write(report_data)
        tmp_path = report.name
    finally:
        if dummy:
            dummy.close()
        if report:
            report.close()

    os.replace(tmp_path, report_path)
    LOGGER.info('Created report.')
    LOGGER.info('Report path: {}'.format(report_path))


def configurate_logger(logger, file_path=None):
    LOGGER.setLevel(logging.DEBUG)
    log_format = logging.Formatter(
        fmt='[%(asctime)s] %(levelname).1s %(message)s',
        datefmt='%Y.%m.%d %H:%M:%S'
    )
    if file_path:
        log_handler = logging.FileHandler(filename=file_path, encoding='UTF-8')
    else:
        log_handler = logging.StreamHandler()
    log_handler.setFormatter(log_format)
    logger.addHandler(log_handler)


def main(cfg):
    log_dir = cfg.get('LOG_DIR')
    log_dir = os.path.abspath(log_dir)

    log_info = get_last_log(log_dir)

    if not log_info:
        LOGGER.info('Doesn\'t have any logs.')
        return
    LOGGER.debug('Last log got successfully.')

    report_dir = cfg.get('REPORT_DIR')
    report_dir = os.path.abspath(report_dir)

    if is_exist_report(report_dir, log_info.date):
        LOGGER.info('The report already exists.')
        return
    LOGGER.debug('Report doesn\'t exist yet.')

    log_data = extract_log_data(log_info.path, parse_log)

    report_size = cfg.get('REPORT_SIZE')
    statistic = prepare_log_statistic(log_data, report_size)

    create_report(statistic, report_dir, log_info.date)


if __name__ == "__main__":
    args = get_args()
    cfg_path = args.config or CONFIG_PATH
    cfg_path = os.path.abspath(cfg_path)

    if not os.path.isfile(cfg_path):
        LOGGER.exception(
            'Configuration file with path {} doesnt\'t exist.'.format(cfg_path)
        )
        exit(1)

    try:
        prepared_cfg = load_config(cfg_path, config)
    except JSONDecodeError:
        LOGGER.exception('Failed to parse configuration file {}.'.format(cfg_path))
        exit(1)

    logging_path = prepared_cfg.get('LOGGING_PATH')
    logging_path = os.path.abspath(logging_path) if logging_path else None

    configurate_logger(LOGGER, logging_path)

    LOGGER.info('Start running...')
    try:
        main(prepared_cfg)
    except (Exception, KeyboardInterrupt):
        LOGGER.exception('Error')
        exit(1)

    LOGGER.info('End.')
